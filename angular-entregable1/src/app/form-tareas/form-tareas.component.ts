import { Component, OnInit } from '@angular/core';
import { Tarea } from './../models/tarea.model';

@Component({
  selector: 'app-form-tareas',
  templateUrl: './form-tareas.component.html',
  styleUrls: ['./form-tareas.component.css']
})
export class FormTareasComponent implements OnInit {

  tareas: Tarea[];
  listaTareas:string[];
  constructor() { }

  ngOnInit(): void {
    this.tareas = [];
    this.listaTareas = [];
  }

  guardar(observaciones: string) :boolean{
    var tarea1 = <HTMLInputElement> document.getElementById('CheckComprar');
    var isChecked1 = tarea1.checked;
    if(isChecked1 == true){
      this.listaTareas.push("Comprar Mercado");
    }
    var tarea2 = <HTMLInputElement> document.getElementById('CheckEstudiar');
    var isChecked2 = tarea2.checked;
    if(isChecked2 == true){
      this.listaTareas.push('Estudiar');
    }
    var tarea3 = <HTMLInputElement> document.getElementById('CheckPagar');
    var isChecked3 = tarea3.checked;
    if(isChecked3 == true){
      this.listaTareas.push('Pagar Facturas');
    }

    var select = <HTMLInputElement> document.getElementById("inputGroupSelect01");
    var opcion = select.value; 

    this.tareas.push(new Tarea(opcion,this.listaTareas,observaciones))
    console.log(this.tareas);
    this.listaTareas = [];
    return false;
  }

}
