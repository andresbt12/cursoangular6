export class Tarea{
    Dia:string;
    Tareas:string[];
    Observacion:String;

    constructor(dia:string, tareas:string[], observacion:string){
        this.Dia = dia;
        this.Tareas = tareas;
        this.Observacion = observacion;
    }
}