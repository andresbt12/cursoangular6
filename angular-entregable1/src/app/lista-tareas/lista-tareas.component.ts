import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Tarea } from './../models/tarea.model';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {

  @Input() Tarea: Tarea;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  listaTareas:string[];

  constructor() { 
    this.listaTareas = [];
    
  }

  ngOnInit(): void {
  }

}
